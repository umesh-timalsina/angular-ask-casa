import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FirebaseService } from '../firebase.service';

@Component({
  selector: 'app-messages-admin-component',
  templateUrl: './messages-admin-component.component.html',
  styleUrls: ['./messages-admin-component.component.css']
})
export class MessagesAdminComponentComponent implements OnInit {

  messages : Observable<any[]>
  constructor(private firebaseService:  FirebaseService) { 
    
  }

  ngOnInit() {
    this.messages = this.firebaseService.items;
  }

}
