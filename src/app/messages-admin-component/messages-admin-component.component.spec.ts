import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagesAdminComponentComponent } from './messages-admin-component.component';

describe('MessagesAdminComponentComponent', () => {
  let component: MessagesAdminComponentComponent;
  let fixture: ComponentFixture<MessagesAdminComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagesAdminComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesAdminComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
