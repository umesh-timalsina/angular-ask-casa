import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header-component',
  templateUrl: './header-component.component.html',
  styleUrls: ['./header-component.component.css']
})
export class HeaderComponentComponent implements OnInit {
  showNavbar : boolean = false;
  constructor() { }
  ngOnInit() {
  }
  onClickNavbar(){
    console.log('clicked')
    this.showNavbar = !this.showNavbar;
    setTimeout( () => {
      this.showNavbar = !this.showNavbar;
    }, 3000);
  }
}
