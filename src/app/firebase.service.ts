import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable()
export class FirebaseService{
    items: Observable<any[]>;
    constructor(private db: AngularFirestore) {
        this.items = this.db.collection('messages').valueChanges();
        
    }

    storeChatMessages(storageObject){
        this.db.collection("messages")
        .doc( Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)).set(storageObject)
        .then(() => console.log('written'))
    }
}