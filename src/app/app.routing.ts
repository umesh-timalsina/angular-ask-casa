import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { ErrorComponentComponent } from './error-component/error-component.component';
import { ChatTextComponentComponent } from './chat-text-component/chat-text-component.component';
import { VoiceComponent } from './voice/voice.component';
import { AboutmeComponent } from './aboutme/aboutme.component';
import { MessagesAdminComponentComponent } from './messages-admin-component/messages-admin-component.component';

const appRoutes: Routes = [
    {path: '', component: LandingPageComponent},
    {path: 'text', component: ChatTextComponentComponent},
    {path: 'voice', component: VoiceComponent},
    {path: 'about-me', component: AboutmeComponent},
    {path: 'admin-messages', component: MessagesAdminComponentComponent},
    {path: '**', component: ErrorComponentComponent},
] // Define the initial Route

@NgModule({
    imports: [
        // RouterModule.forRoot(appRoutes) // Register
        RouterModule.forRoot(appRoutes, {useHash: true})
    ],
    exports: [RouterModule]
})
export class AppRouting{

}