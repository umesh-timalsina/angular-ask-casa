import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { Teximate, TextAnimation } from 'ngx-teximate';
import { fadeInRight } from 'ng-animate';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit, AfterViewInit {
  
  @ViewChild(Teximate) teximate : Teximate;
  @ViewChild('voice') circle1 : ElementRef; // HoverOver Voice
  @ViewChild('text') circle2: ElementRef;

  title = 'Hello!!!  \n I  am  AskCASA...';
  description = `I was developed by College of Applied Science and arts at SIU Carbondale

                  for assisting new students...
                  Please click on the voice or text links above
                  either to have a conversation or text with me.`;
  // Provides initial landing component
  constructor(private renderer: Renderer2,
              private router : Router) { }

  ngOnInit() {
    
  }

  titleAnimation: TextAnimation = {
    id: 'custom',
    animation: fadeInRight,
    delay: 50,
    type: 'letter'
  };


  titleAnimated(){
    // this.descriptionAnimation = {
    //   id: 'description',
    //   animation: fadeInRight,
    //   delay: 50,
    //   type: 'letter'
    // };
    console.log('finished');
  }

  // Register Animation and Set Time out
  ngAfterViewInit(){
  }

  //Mouse over events
  onMouseOver(e){
    if(e === '1'){
      // console.log(e);
      this.renderer.removeClass(this.circle1.nativeElement, 'circle-text-green');
      this.renderer.addClass(this.circle1.nativeElement, 'circle-text');
    }else if(e === '2'){
      this.renderer.removeClass(this.circle2.nativeElement, 'circle-text-green');
      this.renderer.addClass(this.circle2.nativeElement, 'circle-text');
    }
  }

  //Mouse Leave event
  onMouseLeave(e){
    if(e === '1'){
      // console.log(e);
      this.renderer.removeClass(this.circle1.nativeElement, 'circle-text');
      this.renderer.addClass(this.circle1.nativeElement, 'circle-text-green');
    }else if(e === '2'){
      this.renderer.removeClass(this.circle2.nativeElement, 'circle-text');
      this.renderer.addClass(this.circle2.nativeElement, 'circle-text-green');
    }
  }

  // Navigate to the text page
  navigateToText(){
    this.router.navigate(['text']);
  }

  navigateToVoice() {
    this.router.navigate(['voice']);
  }

}
