import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { DialogFlowService } from './dialogflow.service';
import { AppRouting } from './app.routing';
import { AppComponent } from './app.component';
import { HeaderComponentComponent } from './header-component/header-component.component';
import { ErrorComponentComponent } from './error-component/error-component.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TeximateModule } from 'ngx-teximate';
import { FooterComponentComponent } from './footer-component/footer-component.component';
import { ChatTextComponentComponent } from './chat-text-component/chat-text-component.component';
import { ChatmessageComponent } from './chat-text-component/chatmessage/chatmessage.component';
import { TextMessageService } from './chat-text-component/text-message.service';
import { HttpModule } from '@angular/http';
import { VoiceComponent } from './voice/voice.component';
import { RecordingService } from './recording.service';
import { VoiceService } from './voice/voice.service';
import { AboutmeComponent } from './aboutme/aboutme.component';
import { MessagesAdminComponentComponent } from './messages-admin-component/messages-admin-component.component';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { FirebaseService } from './firebase.service' 



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponentComponent,
    ErrorComponentComponent,
    LandingPageComponent,
    FooterComponentComponent,
    ChatTextComponentComponent,
    ChatmessageComponent,
    VoiceComponent,
    AboutmeComponent,
    MessagesAdminComponentComponent, 
  ],
  imports: [
    BrowserModule, 
    AppRouting, 
    BrowserAnimationsModule,
    TeximateModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),  // imports firebase/firestore, only needed for database features
    AngularFirestoreModule
    
  ],
  providers: [DialogFlowService, TextMessageService, RecordingService, VoiceService, FirebaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
