import { Component, OnInit, OnDestroy } from '@angular/core';
import { RecordingService } from '../recording.service';
import * as RecordRTC from 'recordrtc';
import { DialogFlowService } from '../dialogflow.service';
import { VoiceService } from './voice.service';
@Component({
  selector: 'app-voice',
  templateUrl: './voice.component.html',
  styleUrls: ['./voice.component.css'],
  // providers: [VoiceService]
})
export class VoiceComponent implements OnInit, OnDestroy {

  silenceStatus: string = "Silence";
  recordRTC: any;
  private stream: MediaStream;
  constructor(private voiceService: VoiceService) {
   }


  ngOnInit() {
    this.initializeRecording();
    this.voiceService.voiceMessageSubject.subscribe(
      (data) => {
        var snd = new Audio("data:audio/wav;base64," + data);
        snd.play()

        snd.addEventListener('playing', () => {
          this.silenceStatus = 'Speaking'
        });

        snd.addEventListener('ended', () => {
          this.silenceStatus = 'Silence'
        });

    });
  }


  initializeRecording() {
    let mediaConstraints = {
      video: false, audio: true
    };
    navigator.mediaDevices
      .getUserMedia(mediaConstraints)
      .then(this.successCallback.bind(this), this.errorCallback.bind(this));
  }

  successCallback(stream: MediaStream) {

    var options = {
  
      type: "audio",
      mimeType: "audio/wav",
      desiredSampleRate: 48000,
      recorderType: RecordRTC.StereoAudioRecorder,
      numberOfAudioChannels: 1,
      ignoreMutedMedia: true
    };
    this.stream = stream;
    this.recordRTC = RecordRTC(stream, options);
    console.log(this.recordRTC === undefined);
    this.recordRTC.startRecording();
  }

  errorCallback() {
    //handle error here
  }
  toggleRecording(){
    if(this.silenceStatus === 'Recording' || this.silenceStatus === 'Silence'){
      this.silenceStatus = this.silenceStatus === 'Recording'? 'Silence' : 'Recording';
    }
    if(this.silenceStatus === 'Recording'){
      this.successCallback(this.stream);
    }else if(this.silenceStatus === 'Silence'){
      this.stopRecording();
    }
  }
  stopRecording() {
    console.log(this.recordRTC === undefined);
    this.recordRTC.stopRecording(() => {
      let recordedBlob = this.recordRTC.getBlob();
      
      var reader = new FileReader();
      
      reader.readAsDataURL(recordedBlob); 
      reader.onloadend = () => {
        let base64data = reader.result;                
        // console.log(base64data);
        this.voiceService.sendVoiceMessage(base64data);
      }
      
    });
    let stream = this.stream;
    // stream.getAudioTracks().forEach(track => track.stop());
    // stream.getVideoTracks().forEach(track => track.stop());
  }

  ngOnDestroy(){
    this.stream.getAudioTracks().forEach(track => track.stop());
    this.stream.getVideoTracks().forEach(track => track.stop());
  }

}
