import { Injectable } from '@angular/core';
import { Http, Headers} from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable()
export class DialogFlowService{

    private baseURL: string = "https://api.dialogflow.com/v1/query?v=20150910";
    private token: string = "06d3266de0c34aa4ba05c40ee28c9b57";
    initialized : boolean = false;
    // Initialize with the HttpClient Service
    constructor(private http : Http){
        
    }

    initializeDialogflow(){
        let data = {
            project_id : 'newagent-445ea',
            session_id : Math.random().toString()
        }
        this.initialized = true;
        return this.http.post(
            '/initialize-session',
            data
        )
        
    }


    getResponse(query: string){
        let data = {
            query: query,
            lang: 'en',
            sessionId: Math.random().toString()
        }
        return this.http.
            post(`${this.baseURL}`, data, {headers: this.getHeaders()});
    }

    getHeaders(){
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        return headers;
    }

    getResponseFromFlask(query: string){
        let data = {
            query: query
        }
        return this.http.
            post("/send-message", data);
    }

    getVoiceResponseFromFlask(data){
        console.log(data.split(',')[1]);
        let dataSend = {
            query: data.split(',')[1]
        }
        return this.http.
            post("/send-voice-message", dataSend);
    }
}