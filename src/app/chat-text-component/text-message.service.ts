import { Injectable } from "@angular/core";
import { DialogFlowService } from '../dialogflow.service';
import { Subject } from "rxjs";
import { FirebaseService } from "../firebase.service";

@Injectable()
export class TextMessageService{
    
    public messageSubject : Subject<{type: string, text: string}> = new Subject<{type: string, text: string}>();
    
    constructor(private dfService : DialogFlowService, private firebaseService: FirebaseService){
        this.dfService.initializeDialogflow().subscribe();
     }


    sendMessage(query: string){
       
        console.log(query);
        this.messageSubject.next({type: 'userMsg', text: query});
        this.dfService.getResponseFromFlask(query).subscribe(
                (result) => {
                  console.log(result)
                  let tempResObject = result.json().botMsg;
                  let storageObject = {
                      botMsg: tempResObject,
                      userMsg: query,
                      type: "text",
                      messageID: new Date()
                  }
                  this.firebaseService.storeChatMessages(storageObject);
                  setTimeout(() => {this.messageSubject.next(
                      {
                          type: 'botMsg',
                          text: tempResObject
                      });

                }, 300)
            });
    }
}