import { Component, OnInit, OnDestroy, ElementRef, ViewChild, AfterViewChecked } from '@angular/core';
import { TextMessageService } from '../text-message.service';
import { Subject, Subscription } from 'rxjs';
@Component({
  selector: 'app-chatmessage',
  templateUrl: './chatmessage.component.html',
  styleUrls: ['./chatmessage.component.css'],
})
export class ChatmessageComponent implements OnInit, OnDestroy, AfterViewChecked {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  subscription : Subscription;
  messages = [
    {
      type: 'botMsg',
      text: 'Hello, I am Ask Casa'
    },
  ]
  constructor( private msgService : TextMessageService) { }

  ngOnInit() {
    this.subscription = this.msgService.messageSubject.subscribe(
      (result) => {
        console.log(result)
        this.messages.push(result);
      });
      this.scrollToBottom();
  }

  ngAfterViewChecked() {        
    this.scrollToBottom();        
  } 

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  scrollToBottom(): void {
    try {
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch(err) { }                 
  }

  getDate(){
    return new Date().toLocaleString();
  }
}
