import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TextMessageService } from './text-message.service';

@Component({
  selector: 'app-chat-text-component',
  templateUrl: './chat-text-component.component.html',
  styleUrls: ['./chat-text-component.component.css'],
})
export class ChatTextComponentComponent implements OnInit {
  @ViewChild('f') messageForm: NgForm;

  constructor(private textService: TextMessageService) { }

  ngOnInit() {
  }

  onSendMessage(){
    // console.log(this.messageForm.value.message);
    if(this.messageForm.value.message.length === 0){
      this.messageForm.reset();
      return;
    }
    
    this.textService.sendMessage(this.messageForm.value.message);
    this.messageForm.reset();
  }

}
